#include <Magick++.h>
#include <iostream>
#include <math.h>
#include "main.h"

#define SQUARE(x) (x) * (x)
#define INFINITY 1e8
#define MAX_RECURSION_DEPTH 5

using namespace std;

struct Vec3f
{
public:
	float x, y, z;
	Vec3f() : x(0), y(0), z(0) {}
    Vec3f(float xx) : x(xx), y(xx), z(xx) {}
	Vec3f(float xx, float yy, float zz) : x(xx), y(yy), z(zz) {}
	float norm2() const { return x * x + y * y + z * z; }
	float norm() const { return sqrt(len2()); }
	Vec3f& normalize()
	{
		float norm2 = norm2();
		if(norm2 != 0)
		{
			float invNorm = 1 / sqrt(norm2);
			x *= invNorm, y *= invNorm, z *= invNorm;
		}
		return *this;
	}
	Vec3f operator * (const float &f) { return Vec3f(x * f, y * f, z * f); }
	Vec3f operator * (const Vec3f &v) { return Vec3f(x * v.x, y * v.y, z * v.z); }
	float dot(const Vec3f v) const { return x * v.x + y * v.y + z * v.z; }
	Vec3f operator + (const Vec3f &v) { return Vec3f(x + v.x, y + v.y, z + v.z); }
	Vec3f operator - (const Vec3f &v) { return Vec3f(x - v.x, y + v.y, z + v.z); }
	Vec3f operator - () { return Vec3f(-x, -y, -z); }
};

struct Sphere
{
public:
	Vec3f position;
	Vec3f direction;
    float radius, radius2;
    Vec3f surfaceColor, emissionColor;
    float transparency, reflectivity;
	Sphere(
            Vec3f pos, 
            float r,
            Vec3f sC : position(pos), radius(r), radius2(SQUARE(r)), surfaceColor(sC) {}
            // Vec3f eC,
            // float nt,
            // float nr) : position(pos), radius(r), radius2(SQUARE(r)), surfaceColor(sC), emissionColor(eC), transparency(nt), reflectivity(nr)
	void move(Vec3f m) { position += m; }

    bool doesVectorIntersect(Vec3f &rayorig, Vec3f &raydir, float &intersection1, float &intersection2)
    {
        float origToCenter = this->center - rayorig;
        // distance from the ray origin to the point closest to the center of the sphere
        float distOrigToClosestRayPoint = raydir.dot(origToCenter);
        // if this is smaller than one, then the intersection is on the wrong side of the origin
        if (distOrigToClosestRayPoint < 0)
            return false;
        // distance from the center of the sphere to the closest point of the ray squared
        float distToRay2 = SQUARE(origToCenter) - SQUARE(raydir.dot(origToCenter));
        if (distToRay2 > radius2)
            return false;
        // distance from the ray point closest to the sphere center to the intersections of the ray with the sphere
        float distClosestRayPointToIntersection = sqrt(radius2 - distToRay2);
        intersection1 = distOrigToClosestRayPoint + distClosestRayPointToIntersection;
        intersection2 = distOrigToClosestRayPoint - distClosestRayPointToIntersection;
        return true;
    }
};

Vec3f trace(
		const Vec3f &rayorig,
		const Vec3f &raydir,
		const std::vector<Sphere> &spheres,
		const int &depth
	)
{
	raydir.normalize();
    float nearestIntersection = INFINITY;
    Sphere nearestSphere;

    for (int i = 0; i < spheres.size(); ++i)
    {
        float intersection1, intersection2;
        if (spheres[i].doesVectorIntersect(rayorig, raydir, intersection1, intersection2))
        {
            // in case you are inside the sphere ??
            if (intersection1 < 0)
                intersection2 = intersection1;
            if (intersection1 < nearestIntersection)
            {
                nearestIntersection = intersection1;
                nearestSphere = spheres[i];
            }
        }
    }
	
    // if no sphere has been hit, return the default color
    if (!sphere)
        return Vec3f(0);
    Vec3f intersectionPoint = raydir * nearestIntersection;
    Vec3f intersectionNormal = intersectionPoint - nearestSphere.center;
    intersectionNormal.normalize();

    // Vec3f reflectionVector = raydir + intersectionNormal * raydir.dot(intersectionNormal) * 2;
    // Vec3f refractionVector = 
    return nearestSphere.surfaceColor;
}

void render(vector<Sphere> spheres)
{
    unsigned width = 1000, height = 800;
    Vec3f *image = new Vec3f[width * height];
    // vertical field of view in angles
    float fov = 30,
          aspectRatio = width / float(height),
          heightToDepth = tan((0.5 * M_PI) * (fov / 180));
	// shooting all the rays
	for (int y = 0; y < width; y++)
	{
		for (int x = 0; x < height; x++)
		{
            float xDirection = (2 * (x + 0.5) / width - 1) * heightToDepth * aspectRatio;
            float yDirection = (1 - 2 * (x + 0.5) / height) * heightToDepth;
			Vec3f rayOrigin(0);
            Vec3f rayDirection(xDirection, yDirection, -1);
			ray.normalize();
            image[x][y] = trace(rayOrigin, raydir, spheres, 0);
		}
	}
    a
}

int main()
{
    vector<Sphere> spheres;

    spheres.push_back(Sphere(Vec3f(0.0, 0.0, -20), 5, Vec3f(255, 0, 0)));
    spheres.push_back(Sphere(Vec3f(10.0, 7.0, -35), 7, Vec3f(0, 0, 255)));
    render(spheres);
    return 0;
}
